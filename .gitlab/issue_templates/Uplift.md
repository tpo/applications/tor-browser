<!--
Title:
    Uplift tor-browser#12345: Title of Issue

This is an issue for tracking uplift of a patch-set to Firefox
-->

## Uplift Patchset

### Book-keeping

#### Gitlab Issue(s)
- tor-browser#12345
- mullvad-browser#123

#### Merge Request(s)
- tor-browser!123

#### Upstream Mozilla Issue(s):
- https://bugzilla.mozilla.org/show_bug.cgi?id=12345

### Notes

<!-- whatever additional info, context, etc that would be helpful for uplifting -->

/label ~"Apps::Type::Uplift"
