/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

static const char* kBaseFonts[] = {
    "Apple Color Emoji",
    "AppleGothic",
    "Arial",
    "Arial Black",
    "Arial Narrow",
    "Courier",
    "Courier New",
    "Geneva",
    "Georgia",
    "Heiti TC",
    "Helvetica",
    "Helvetica Neue",
    "Hiragino Kaku Gothic ProN",
    "Kailasa",
    "Lucida Grande",
    "Menlo",
    "Monaco",
    "PingFang HK",
    "PingFang SC",
    "PingFang TC",
    "Songti SC",
    "Songti TC",
    "Tahoma",
    "Thonburi",
    "Times",
    "Times New Roman",
    "Verdana",
};
